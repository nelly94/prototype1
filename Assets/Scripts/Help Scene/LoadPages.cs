﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data
using UnityEngine;

public class LoadPages : MonoBehaviour
{
	//public GameObject[] pages;
	public GameObject[] helpPages;
	private int setIndex;

	private string sceneName;
	private string changingSceneName;
	private FadeScreen fadeScreen;

	private GameManager gameManager;
	private InstructionsPanel instructionPanel;

	private UISoundManager uiSoundManager;


	void Awake()
	{
		sceneName = SceneManager.GetActiveScene ().name;
		uiSoundManager = FindObjectOfType<UISoundManager> ();
		fadeScreen = GameObject.FindWithTag ("FadeTo").GetComponent<FadeScreen> ();


		if ((sceneName == "Help2") || (sceneName == "09-03")) 
		{
			gameManager = FindObjectOfType<GameManager> ();

			for (int index = 0; index < helpPages.Length; index++) 
			{

				//helpPages [index].GetComponent<SpriteRenderer>().enabled = false;
				helpPages [index].gameObject.SetActive(false);
			}

		}
		if (sceneName == "09-03")
			instructionPanel = GameObject.FindWithTag ("InstructionPanel").GetComponent<InstructionsPanel> ();
	}

	void Start()
	{
		if (sceneName == "Help2") 
		{
			setIndex = 0;
			helpPages [setIndex].gameObject.SetActive (true);
		}
	}

	void Update()
	{
		if ((sceneName == "Help2") || (sceneName == "09-03")) 
		{

			if ((Input.GetKeyDown (KeyCode.Keypad1) || Input.GetKeyDown (KeyCode.Alpha1)) || (Input.GetKeyDown (KeyCode.Keypad2) || Input.GetKeyDown (KeyCode.Alpha2)))
				PageOne_Two ();
			if ((Input.GetKeyDown (KeyCode.Keypad3) || Input.GetKeyDown (KeyCode.Alpha3)) || (Input.GetKeyDown (KeyCode.Keypad4) || Input.GetKeyDown (KeyCode.Alpha4)))
				PageThree_Four ();
			if ((Input.GetKeyDown (KeyCode.Keypad5) || Input.GetKeyDown (KeyCode.Alpha5)) || (Input.GetKeyDown (KeyCode.Keypad6) || Input.GetKeyDown (KeyCode.Alpha6)))
				PageFive_Six ();
			if (Input.GetKeyDown (KeyCode.Keypad7) || Input.GetKeyDown (KeyCode.Alpha7))
				PageSeven ();
			if (Input.GetKeyDown (KeyCode.Keypad0) || Input.GetKeyDown (KeyCode.Alpha0))
				ContentsPage ();
			if (Input.GetKeyDown (KeyCode.LeftArrow))
				PreviousPage ();
			if (Input.GetKeyDown (KeyCode.RightArrow))
				NextPage ();
			// ESC
			if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				if (sceneName == "Help2")
					backToMenu ();
				if (sceneName == "09-03")
					backToGame ();	
			}				
		}
		else
		{
			if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				if (sceneName == "GameOver" || sceneName == "Victory") 
					backToMenu ();
			}
		}

		Debug.Log (setIndex);
	}

	public void NextPage()
	{
		if (setIndex < (helpPages.Length - 1)) 
		{
			helpPages [setIndex].gameObject.SetActive (false);
			setIndex++;
			uiSoundManager.paperTurnSound ();
			helpPages [setIndex].gameObject.SetActive (true);		
		}
	}

	public void PreviousPage()
	{
		if (setIndex > 0) 
		{

			helpPages [setIndex].gameObject.SetActive (false);
			setIndex--;
			uiSoundManager.paperTurnSound ();
			helpPages [setIndex].gameObject.SetActive (true);		
		}
	}

	public void ContentsPage()
	{
		helpPages [setIndex].gameObject.SetActive (false);
		setIndex = 0;
		uiSoundManager.paperTurnSound ();
		helpPages [setIndex].gameObject.SetActive (true);
	}

	public void PageOne_Two()
	{
		helpPages [setIndex].gameObject.SetActive (false);
		setIndex = 1;
		uiSoundManager.paperTurnSound ();
		helpPages [setIndex].gameObject.SetActive (true);
	}
	public void PageThree_Four()
	{
		helpPages [setIndex].gameObject.SetActive (false);
		setIndex = 2;
		uiSoundManager.paperTurnSound ();
		helpPages [setIndex].gameObject.SetActive (true);
	}
	public void PageFive_Six()
	{
		helpPages [setIndex].gameObject.SetActive (false);
		setIndex = 3;
		uiSoundManager.paperTurnSound ();
		helpPages [setIndex].gameObject.SetActive (true);
	}
	public void PageSeven()
	{
		helpPages [setIndex].gameObject.SetActive (false);
		setIndex = 4;
		uiSoundManager.paperTurnSound ();
		helpPages [setIndex].gameObject.SetActive (true);
	}
		
	public void backToMenu()
	{
		changingSceneName = "menuattemp1";
		StartCoroutine (ChangeScenes());
		//SceneManager.LoadScene ("menuattemp1");
	}

	public void backToGame()
	{
		instructionPanel.panelOut ();
	}
		
	private IEnumerator ChangeScenes()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		SceneManager.LoadScene (changingSceneName);
		//SceneManager.LoadScene ("menuattemp1");
	}

	//Do this when the cursor enters the rect area of this selectable UI object.
	//public void OnPointerEnter(PointerEventData eventData)
	//{
		//Cursor.SetCursor (cursorHandler.cursorHand, Vector2.zero, CursorMode.Auto);
	//}
	//public void OnPointerExit(PointerEventData eventData)
	//{
	//	Cursor.SetCursor (cursorHandler.cursorArrow, Vector2.zero, CursorMode.Auto);
	//}


}
	