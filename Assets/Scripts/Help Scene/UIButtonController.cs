﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data
using UnityEngine;

public class UIButtonController : MonoBehaviour, IPointerEnterHandler , IPointerExitHandler
{
	private CursorHandler cursorHandler;
	private UISoundManager uiSoundManager;

	void Awake()
	{
		cursorHandler = GameObject.FindWithTag ("CursorHandler").GetComponent<CursorHandler> ();
		//cursorHandler = FindObjectOfType<CursorHandler> ();

		uiSoundManager = FindObjectOfType<UISoundManager> ();

		//audioSource = this.gameObject.GetComponent<AudioSource> ();
	}

	//Do this when the cursor enters the rect area of this selectable UI object.
	public void OnPointerEnter(PointerEventData eventData)
	{
		Cursor.SetCursor (cursorHandler.cursorHand, Vector2.zero, CursorMode.Auto);
		if(this.gameObject.tag == "PaperButtons")
			uiSoundManager.paperTapSound ();
		if (this.gameObject.tag == "WoodenButtons")
			uiSoundManager.woodSound ();
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
	}
}
