﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PageJump : MonoBehaviour 
{ 
	private LoadPages loadPages;

	void Awake ()
	{
		loadPages = FindObjectOfType<LoadPages> ();

		//GameObject.FindGameObjectsWithTag ("Page1-2");
		//GameObject.FindGameObjectsWithTag ("Page3-4");
		//GameObject.FindGameObjectsWithTag ("Page5-6");
		//GameObject.FindGameObjectsWithTag ("Page7");



	}

	void OnMouseDown()
	{
		if (CompareTag ("NextPage"))
			loadPages.NextPage ();			
		if (CompareTag ("PreviousPage"))
			loadPages.PreviousPage ();
		if (CompareTag ("Contents"))
			loadPages.ContentsPage ();
		if (CompareTag ("Page1-2"))
			loadPages.PageOne_Two ();
		if (CompareTag ("Page3-4"))
			loadPages.PageThree_Four ();
		if (CompareTag ("Page5-6"))
			loadPages.PageFive_Six ();
		if (CompareTag ("Page7"))
			loadPages.PageSeven ();
		if (CompareTag ("ExitHelp")) 
		{
			//SceneManager.LoadScene ("menuattemp1");
		}
	}

}


/*
void Update()
	{
		
			if ((Input.GetMouseButtonUp (0) && CompareTag ("NextPage")))
				loadPages.NextPage ();	
			if ((Input.GetMouseButtonDown (0) && CompareTag ("PreviousPage")))
				loadPages.PreviousPage ();
			if ((Input.GetMouseButtonDown (0) && CompareTag ("Contents")))
				loadPages.ContentsPage ();
			if ((Input.GetMouseButtonDown (0) && CompareTag ("Page1-2")))
				loadPages.PageOne_Two ();
			if ((Input.GetMouseButtonDown (0) && CompareTag ("Page3-4")))
				loadPages.PageThree_Four ();
			if ((Input.GetMouseButtonDown (0) && CompareTag ("Page5-6")))
				loadPages.PageFive_Six ();
			if ((Input.GetMouseButtonDown (0) && CompareTag ("Page7")))
				loadPages.PageSeven (); 
			//if ((Input.GetMouseButtonDown (0) && CompareTag ("ExitHelp")))
				//SceneManager.LoadScene ("menuattemp1");
	}

void OnMouseUp()
	{
		if (CompareTag ("NextPage"))
			loadPages.NextPage ();			
		if (CompareTag ("PreviousPage"))
			loadPages.PreviousPage ();
		if (CompareTag ("Contents")) 
			loadPages.ContentsPage ();
		if (CompareTag ("Page1-2")) 
			loadPages.PageOne_Two ();
		if (CompareTag ("Page3-4")) 
			loadPages.PageThree_Four ();
		if (CompareTag ("Page5-6")) 
			loadPages.PageFive_Six ();
		if (CompareTag ("Page7"))
			loadPages.PageSeven ();
		if (CompareTag ("ExitHelp")) 
		{
			SceneManager.LoadScene ("menuattemp1");
			menuSet.goneToHelp = true;

		}
 */