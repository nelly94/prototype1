﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;


public class LanternController : MonoBehaviour 
{
	// PLAYER INPUT
	//private LightManager lightManager;
	public PlayerSounds playerSounds;
	// LIGHT 
	public GameObject lantern;
	public Light lightSource;
	public GameObject visibility;
	public bool lightOut;
	public float lastRange;
	// light range
	public float maxRange;
	public float minRange;
	public float incrementationsAllowed;
	private float lightIncrementation;
	//private float lightIncrementation = maxRange - min/incrementationsAllowed;

	// FLINT
	public int flintAmount; 
	public int flintMaxHold;
	public int[] flintStrikeAmount = {0,0,0}; // An array that stores the strike amount of each flint picked up. // 3 is the flintmaxhold
	private int index;
	private int sum;

	//public int flintStrikeAmount;

	// OIL
	public float oilAmount;
	public float oilMaxHold; // = 50f;
	public float oilDepletion;
	public float drainSpeed;	 

	// UI
	public GameObject[] flintRepresentation;
	//public Image oilBackground;
	public Image oilfill;
	//public bool oilAmountFill;

	private FadeScreen fadeScreen;
	private string sceneName;

	private TutorialPanel tutorialPanel;
	private LoosePages loosePage;

	private InfoPanel infoPanel;


	void Awake()
	{
		// Sound
		playerSounds = GameObject.FindWithTag("Player").GetComponent<PlayerSounds>();

		fadeScreen = GameObject.FindWithTag ("FadeTo").GetComponent<FadeScreen> ();

		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();
		loosePage = tutorialPanel.gameObject.GetComponentInChildren<LoosePages> ();

		// LIGHT
		lightOut = false;
		// light range 
		//maxRange = 4f;
		//minRange = 2f;
		incrementationsAllowed = 8f;
		lightIncrementation = (maxRange - minRange) /incrementationsAllowed;
		//private float lightIncrementation = maxRange/incrementationsAllowed;

		//FLINT
		flintMaxHold  = 3;
		index = 0;

		//OIL
		drainSpeed = 0.02f;

		infoPanel = GameObject.FindWithTag("InfoPanel").GetComponent<InfoPanel>();
	}

	void FixedUpdate ()
	{
		if (flintAmount > 0)
			infoPanel.flintGlow.SetActive (true);
		else 
			infoPanel.flintGlow.SetActive (false);
		
		if (oilAmount > 0)
			infoPanel.oilGlow.SetActive (true);
		else
			infoPanel.oilGlow.SetActive (false);
		
		if (!GameManager.Instance.Paused) 
		{

			// StrikeFlint Control 
			// checks to see any open positions and will either sets array to first position, or cycles through until open position
			if ((sum == 0) || (index > 2))
				index = 0;
			else if (flintStrikeAmount [index] == 0)
				index++;

			// ____LIGHT CONTROL____
			// check if the lantern has been collected 
			if (lantern == null) 
			{	
				oilDepletion = drainSpeed * (lightSource.range * 2.5f);
				//visibility.transform.localScale =  new Vector3((lightSource.range *2f), (6.5f), (lightSource.range *2f));
				visibility.transform.localScale = new Vector3 ((lightSource.range * 2f), (6.5f), ((lightSource.range * 2f) + 1.5f)); // Due to maskCutawayDistance (FieldOfView)

				// UI
				oilfill.fillAmount = oilAmount / oilMaxHold;

				if (lightSource.enabled) 
				{	
					// scroll up - increase the intensity
					if (lightSource.range < maxRange) 
					{
						if ((Input.GetAxis ("Mouse ScrollWheel")) > 0f || Input.GetKeyDown (KeyCode.UpArrow)) 
						{
							playerSounds.PlayLanternTickingSound ();						
							lightSource.range += lightIncrementation;
						}
					}
					// scroll down - decrease the intensity
					if ((Input.GetAxis ("Mouse ScrollWheel")) < 0f || Input.GetKeyDown (KeyCode.DownArrow)) 
					{
						if ((lightSource.range - lightIncrementation) < minRange)
							lightSource.range = 0f;
						else 
						{
							playerSounds.PlayLanternTickingSound ();
							lightSource.range -= lightIncrementation;
						}
					}
				
					if (lightSource.range == 0f)
						lightSource.enabled = false;

					if (oilAmount > 0)
						oilAmount -= oilDepletion * Time.deltaTime;
					if (oilAmount <= 0) 
					{
						lightOut = true;
						lastRange = lightSource.range;
						lightSource.enabled = false;
						lightSource.range = 0f;
					}
				}
			}
		}
	}

	public void PlayerLightOn()
	{

		//if (((Input.GetAxis ("Mouse ScrollWheel")) > 0f) && (flintAmount > 0) && (oilAmount > 0)) 
		//if ((Input.GetMouseButtonDown (0)) && (flintAmount > 0) && (oilAmount > 0)) 
		if (lantern == null) 
		{
			if (!lightSource.enabled) 
			{
				//if ((Input.GetMouseButtonDown (0)) && (flintAmount > 0) && (oilAmount > 0)) 
				if ((flintAmount > 0) && (oilAmount > 0)) 
				{
					StrikeFlint ();
					lightSource.enabled = true;
					if (lightOut) 
					{
						lightSource.range = lastRange;
						lightOut = false; 
					} 
					else if (lightSource.range == 0f)
						lightSource.range = minRange;
				} 
				else 
				{
					lightSource.enabled = false;
					lightSource.range = 0f;
					visibility.transform.localScale = new Vector3 (1f, 1f, 1f); // Due to maskCutawayDistance (FieldOfView)
				}
			}
		}
	}

	/*
	IEnumerator StartFireSound()
	{
		// Flint
		float volume = Random.Range(0.6f, 0.8f);
		playerSounds.audioSource.clip = playerSounds.flintSound[Random.Range(0,playerSounds.flintSound.Length)];
		playerSounds.audioSource.PlayOneShot (playerSounds.audioSource.clip, volume);
		yield return new WaitForSeconds(playerSounds.audioSource.clip.length);
		// FireIgnite
		playerSounds.audioSource.clip = playerSounds.fireSound[Random.Range(0,playerSounds.fireSound.Length)];
		playerSounds.audioSource.PlayOneShot (playerSounds.audioSource.clip, volume);
	}
	*/

	// FLINT
	// controls the flint usage 
	public void StrikeFlint()
	{
		//Sound
		//StartCoroutine(StartFireSound());
		playerSounds.PlayFlintSound();
	
		for (int i = 0; i < flintStrikeAmount.Length; i++)
		{
			sum += flintStrikeAmount[i];
		}	

		if (index <= 2) 
		{
			//if (flintStrikeAmount [index] == 0)
			//index++;
			if (flintStrikeAmount [index] > 0) 
			{
				flintStrikeAmount [index]--;
				//Rescale ();
				flintRepresentation[index].transform.localScale = new Vector3((flintStrikeAmount[index]/3f) , (flintStrikeAmount[index]/3f) , 1f);
				if (flintStrikeAmount [index] == 0) 
				{
					flintAmount--;
					index++;
				}
			}
		} 

	/*
	 // moved to if (!lightSource.enabled)
		if (lightOut == true) 
		{
			
			lightSource.range = lastRange;
			lightOut = false; 
		} 
		else if (lightSource.range == 0f)
			lightSource.range = 0.375f;
			//lightSource.range = 1.125f;
	*/

	}

	/*
	public void Rescale()
	{
		//flintRepresentation[index].transform.localScale = new Vector3((flintStrikeAmount[index]/3f) , (flintStrikeAmount[index]/3f) , 1f);
		// three is the max strikes a flint can have

		//this is hard coded
		if (flintStrikeAmount [index] == 3)
			flintRepresentation[index].transform.localScale = new Vector3(1f, 1f , 1f);
		else if (flintStrikeAmount [index] == 2)
			flintRepresentation[index].transform.localScale = new Vector3(0.7f, 0.7f , 1f);
		else if (flintStrikeAmount [index] == 1)
			flintRepresentation[index].transform.localScale = new Vector3(0.45f, 0.45f , 45f);
		else 
			flintRepresentation[index].transform.localScale = new Vector3(0, 0 , 0);

	}
	*/

	public void PlayerLightOff()
	{
		if (lantern == null) 
		{
			if (lightSource.enabled) 
			{
				// Sound
				playerSounds.PlayBlowOutSound();

				//playerSounds.audioSource.clip = playerSounds.blowOutSound[Random.Range(0,playerSounds.blowOutSound.Length)];
				//playerSounds.audioSource.Play ();
				// blow out the light - turns out the light and stores the last value of the light's range.
				//if (Input.GetMouseButtonDown (1)) 
				//{
					lightOut = true; 
					lastRange = lightSource.range; // store the value
					lightSource.enabled = false;
					lightSource.range = 0f;
				//}	
			}
		}
	}


	private IEnumerator ChangeScenes()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		SceneManager.LoadScene (sceneName);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Door(Exit)") 
		{
			sceneName = "Victory";
			StartCoroutine (ChangeScenes());
		}
		if (other.gameObject.tag == "Detector2") 
		{
			if (!loosePage.page6Seen) 
			{
				if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
				{
					tutorialPanel.panelIn ();
					loosePage.LoosePageSix ();
					loosePage.page6Seen = true;
					//Destroy (other.gameObject);
				}
			} 
			else 
			{
				if (!loosePage.loosePages [5].gameObject.activeSelf)
					Destroy (other.gameObject);
			}
		}
		if (other.gameObject.tag == "Detector3") 
		{
			Debug.Log ("Im in");
			if (!loosePage.page7Seen) 
			{
				if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
				{
					tutorialPanel.panelIn ();
					loosePage.LoosePageSeven ();
					loosePage.page7Seen = true;
					//Destroy (other.gameObject);
				}
			} 
			else 
			{
				if (!loosePage.loosePages [6].gameObject.activeSelf)
					Destroy (other.gameObject);
			}
		}

	}

	/*
	// ____COLLECTION____
	void OnTriggerEnter(Collider other)
	{
		// detect the trigger of the lantern - if collided with collect the lantern and make a light component within the player.
		if (other.gameObject == lantern)
		{
			//other.gameObject.SetActive(false); // deactivates but does not destroy gameObject
			Destroy(other.gameObject);

			// ____LIGHT____
			// ADDING A LIGHT SOURCE
			//lightSource = gameObject.AddComponent<Light>();
			lightSource.enabled = true;
			//lightSource.color = Color.white;
			lightSource.intensity = 8.0f;
			lightSource.enabled = false;

			//UI
			//oilBackground.GetComponent<Image>().color = new Color32(34,34,34,255);
			//oilBackground.color = new Color32(34,34,34,255);
		}
	}
	*/
}