﻿// ____ REFERENCES ____
// adapted from inScope Studios's "Unity tutorial: Pausing a game" tutorial https://www.youtube.com/watch?v=z5oA-DvVGoo&feature=youtu.be#t=5m15s

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public bool paused;
	public bool Paused
	{
		get
		{
			return paused;
		}
	}

	private static GameManager instance;
	public static GameManager Instance
	{
		get
		{
			if (instance == null) 
			{
				instance = GameObject.FindObjectOfType<GameManager> ();
			}
			return GameManager.instance;
		}
	}

	//void Awake()
	//{
		//enemies = GameObject.FindGameObjectsWithTag("Enemy");
	//}

	public void PauseGame()
	{
		paused = !paused;
	}


}

