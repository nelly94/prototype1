﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour 
{
	public AudioClip[] flintSound;
	public AudioClip[] blowOutSound;
	public AudioClip[] lanternTickingSound;
	public AudioClip[] fireSound;

	// Pick Up Sounds
	public AudioClip lanternPickUp;
	public AudioClip[] flintPickUp;
	public AudioClip[] oilPickUp;

	public float delayTime;


	public float volume;

	public AudioSource audioSource;

	void Awake()
	{
		audioSource = this.gameObject.GetComponent<AudioSource> ();
	}

	public void PlayFlintSound()
	{
		volume = Random.Range(0.8f, 0.95f);
		audioSource.clip = flintSound[Random.Range(0,flintSound.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

	public void PlayBlowOutSound()
	{
		volume = Random.Range(0.9f, 1f);
		audioSource.clip = blowOutSound[Random.Range(0,blowOutSound.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

	public void PlayLanternTickingSound()
	{
		volume = Random.Range(0.6f, 0.95f);
		audioSource.clip = lanternTickingSound[Random.Range(0,lanternTickingSound.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

	public void lanternPickUpSound ()
	{
		volume = 1f;
		audioSource.clip = lanternPickUp;
		audioSource.PlayOneShot (audioSource.clip, volume);
	}	
	public void flintPickUpSound ()
	{
		volume = Random.Range(0.8f, 0.95f);
		audioSource.clip = flintPickUp[Random.Range(0,flintPickUp.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
		Debug.Log ("flintsound");
	}	
	public void oilPickUpSound (int track)
	{
		volume = Random.Range(0.75f, 0.95f);
		//audioSource.volume = volume;
		audioSource.clip = oilPickUp[track];
		audioSource.PlayOneShot (audioSource.clip, volume);
		//audioSource.PlayDelayed(delayTime);
		Debug.Log ("oilsound");
	}
}

/*
playerSounds.audioSource.clip = playerSounds.flintSound[Random.Range(0,playerSounds.flintSound.Length)];
playerSounds.audioSource.Play ();

public void Sound( AudioClip[] soundToPlay)
	{
		volume = 1.0f;
		volume = Random.Range(0.6f, 0.8f);

		audioSource.clip = this.soundToPlay[Random.Range(0,this.soundToPlay.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

*/