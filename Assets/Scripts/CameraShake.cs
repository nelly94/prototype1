﻿// adapted from WILEz75 on ftvs' "CameraShake.cs" script from https://gist.github.com/ftvs/5822103

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour 
{

	public Transform cameraTransform;

	// How long the object should shake for.
	public float shakeDuration;

	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount;
	public float decreaseFactor;

	public bool shaking = false;

	Vector3 originalPosition;
	float originalShakeDuration; //<--add this

	void Awake()
	{
	    if (cameraTransform == null)
				cameraTransform = this.transform;
	}

	void OnEnable()
	{
	    originalPosition = cameraTransform.localPosition;
	    originalShakeDuration = shakeDuration; 
	}

	void Update()
	{
		if (shaking)
	    {
	        if (shakeDuration > 0)
	        {
	            cameraTransform.localPosition = originalPosition + Random.insideUnitSphere * shakeAmount;

	            shakeDuration -= Time.deltaTime * decreaseFactor;
	        }
	        else
	        {
	            shakeDuration = originalShakeDuration; 
	            cameraTransform.localPosition = originalPosition;
				shaking = false;
	        }
	    }
	}

	public void shakecamera()
	{
		shaking = true;
	}
}

/*

// adapted from Sebastian Lague's "[Unity] Camera Shake" tutorial from https://www.youtube.com/watch?v=D3gbXAVcG-0

public Settings testSettings;

	IEnumerator Shake( Settings settings) // co-routine
	{
		float completionPercentage = 0;
		float movePercentage = 0;	// how far camera is between waypoints

		float angle = settings.angle * Mathf.Deg2Rad; // in radians 
		Vector3 previousWaypoint = Vector3.zero;
		Vector3 currentWaypoint = Vector3.zero;
		float moveDistance = 0;// distance between way points

		while (true) 
		{
			completionPercentage += Time.deltaTime / settings.duration; 
			movePercentage += Time.deltaTime / moveDistance * settings.speed; // maintain constant speed
			transform.localPosition = Vector3.Lerp(previousWaypoint,currentWaypoint,movePercentage); // move camera to current waypoint
			if (movePercentage >= 1) 
			{
				float noiseAngle = (Random.value - 0.5f) * Mathf.PI; // inside a semi circle
				angle += Mathf.PI + noiseAngle * settings.noisePercentage;
				///////////// currentWaypoint = new Vector3(Mathf.Cos(angle_radians), 
			}

			yield return null; // waits for a frame  between each execution of each while loop
		}

	}

	[System.Serializable]
	public class Settings
	{
		public float angle;
		public float strength;
		public float speed;
		public float duration;

		[Range (0,1)]
		public float noisePercentage;
		[Range (0,1)]
		public float  dampingPercentage;
		[Range (0,1)]
		public float  roatationPercentage;
	}

*/