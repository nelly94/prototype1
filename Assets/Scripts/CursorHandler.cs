﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorHandler : MonoBehaviour 
{
	//public Texture2D cursor;
	public Texture2D cursorHand;
	public Texture2D cursorArrow;

	void Awake()
	{
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
	}

}
