﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepsManager : MonoBehaviour 
{
	public AudioSource audioSource;
	public AudioClip[] footstepsSounds;

	public float volume;


	void Awake()
	{
		audioSource = this.gameObject.GetComponent<AudioSource> ();
	}

	void Start()
	{
		audioSource.enabled = true;
	}

	public void FootSteps()
	{
		volume = 1.0f;
		volume = Random.Range(0.79f, 0.98f);

		audioSource.clip = this.footstepsSounds[Random.Range(0,this.footstepsSounds.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}
}
