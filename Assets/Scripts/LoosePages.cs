﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoosePages : MonoBehaviour 
{
	public GameObject[] loosePages;

	private TutorialPanel tutorialPanel;
	private GameManager gameManager;
	private InstructionsPanel instructionPanel;
	private LoadPages loadPages;

	private UISoundManager uiSoundManager;

	private int setIndex;

	public bool page1Seen = false;
	public bool page2Seen = false;
	public bool page3Seen = false;
	public bool page4Seen = false;
	public bool page5Seen = false;
	public bool page6Seen = false;
	public bool page7Seen = false;

	void Awake()
	{
		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		instructionPanel = GameObject.FindGameObjectWithTag ("InstructionPanel").GetComponent<InstructionsPanel> ();
		loadPages = GameObject.FindGameObjectWithTag ("InstructionPanel").GetComponentInChildren<LoadPages> ();

		uiSoundManager = FindObjectOfType<UISoundManager> ();

		for (int index = 0; index < loosePages.Length; index++) 
		{
			loosePages [index].gameObject.SetActive(false);
		}
	}

	void Start()
	{
		//setIndex = 0;
		//loosePages [setIndex].gameObject.SetActive (true);
	}

	void Update()
	{
		//if (this.gameObject.transform.GetChild (0).gameObject.activeSelf) 
		//{
			if (Input.GetKeyDown (KeyCode.Escape))
				backToGame ();
		//}
	}

	// Alternate between which item will display 
	public void LoosePageOne()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 0;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page1Seen = true;
	}
	public void LoosePageTwo()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 1;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page2Seen = true;
	}
	public void LoosePageThree()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 2;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page3Seen = true;
	}
	public void LoosePageFour()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 3;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page4Seen = true;
	}
	public void LoosePageFive()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 4;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page5Seen = true;
	}
	public void LoosePageSix()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 5;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page6Seen = true;
	}
	public void LoosePageSeven()
	{
		loosePages [setIndex].gameObject.SetActive (false);
		setIndex = 6;
		uiSoundManager.paperPickUpSound ();
		loosePages [setIndex].gameObject.SetActive (true);
		//page7Seen = true;
	}
	public void backToGame()
	{
		uiSoundManager.paperPickUpSound ();
		tutorialPanel.panelOut ();
		Debug.Log("Ive been clicked");
	}
	// If any of the bold words are clicked, it will open up the help book to that page
	public void HelpBookContentsPage()
	{
		tutorialPanel.panelOut ();
		instructionPanel.panelIn ();
		loadPages.ContentsPage ();
	}
	public void HelpBookPageOne_Two()
	{
		tutorialPanel.panelOut ();
		//tutorialPanel.tutorialPanelIn = false;
		instructionPanel.panelIn ();
		loadPages.PageOne_Two();
	}
	public void HelpBookPageThree_Four()
	{
		tutorialPanel.panelOut ();
		//tutorialPanel.tutorialPanelIn = false;
		instructionPanel.panelIn ();
		loadPages.PageThree_Four ();
	}
	public void HelpBookPageFive_Six()
	{
		tutorialPanel.panelOut ();
		//tutorialPanel.tutorialPanelIn = false;
		instructionPanel.panelIn ();
		loadPages.PageFive_Six ();
	}
	public void HelpBookPageSeven()
	{
		tutorialPanel.panelOut ();
		//tutorialPanel.tutorialPanelIn = false;
		instructionPanel.panelIn ();
		loadPages.PageSeven ();
	}
}
