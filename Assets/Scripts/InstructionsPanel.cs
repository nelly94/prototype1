﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InstructionsPanel : MonoBehaviour 
{
	private GameManager gameManager;
	private MenuPanel menuPanel;
	private TutorialPanel tutorialPanel;

	private LoadPages loadPage;

	public bool helpPanelIn;


	void Awake ()
	{
		//gameManager = FindObjectOfType<GameManager> (); 
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		menuPanel = GameObject.FindGameObjectWithTag("EscMenu").GetComponent<MenuPanel>();
		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();

		loadPage = this.gameObject.GetComponentInChildren<LoadPages> ();

		loadPage.gameObject.transform.GetChild(0).gameObject.SetActive (false);
		loadPage.enabled = false;
	}


	public void Update () 
	{
		if (!menuPanel.menuPanelIn && (!tutorialPanel.tutorialPanelIn)) 
		{
			//helpPanelIn = (transform.GetChild (0).gameObject.activeSelf) ? true : false; 
			helpPanelIn = (loadPage.gameObject.transform.GetChild (0).gameObject.activeSelf) ? true : false;

			if (Input.GetKeyDown (KeyCode.Slash) || Input.GetKeyDown (KeyCode.Question)) 
			{
				//gameManager.PauseGame ();
				if (!helpPanelIn)
					panelIn ();
				else
					panelOut ();
			}
		}
	}

	public void panelIn()
	{
		gameManager.PauseGame ();
		//transform.GetChild (0).gameObject.SetActive(true);
		loadPage.enabled = true;
		loadPage.gameObject.transform.GetChild(0).gameObject.SetActive(true);

		loadPage.ContentsPage ();

	}

	public void panelOut()
	{
		gameManager.PauseGame ();
		loadPage.gameObject.transform.GetChild(0).gameObject.SetActive (false);

		for (int index = 0; index < loadPage.helpPages.Length; index++) 
		{
			loadPage.helpPages [index].gameObject.SetActive (false);
		}
		loadPage.ContentsPage ();
		loadPage.enabled = false;
	}
}
