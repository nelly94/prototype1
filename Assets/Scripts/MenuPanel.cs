﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour 
{

	private GameManager gameManager;
	private InstructionsPanel instructionPanel;
	private TutorialPanel tutorialPanel;

	public bool gamePaused = false;


	//private Animator animator;
	public bool menuPanelIn;
	//public bool hasBeenSeen;

	void Awake ()
	{
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		instructionPanel = GameObject.FindGameObjectWithTag ("InstructionPanel").GetComponent<InstructionsPanel> ();
		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();

		transform.GetChild (0).gameObject.SetActive(false);
	}

	public void Update () 
	{
		//bool menuAvaliable;
		if (!instructionPanel.helpPanelIn && (!tutorialPanel.tutorialPanelIn))
		{
			menuPanelIn = (transform.GetChild (0).gameObject.activeSelf) ? true : false; 

			if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				if (!menuPanelIn)
					MenuPanelIn ();
				else
					MenuPanelOut ();

			}
		}
	}

	public void MenuPanelIn()
	{
		if (!gamePaused) 
		{
			transform.GetChild (0).gameObject.SetActive (true);
			gameManager.PauseGame ();
			gamePaused = true;
		}
	}

	public void MenuPanelOut()
	{
		SetUnactive ();
	}

	public void backToGame()
	{
		MenuPanelOut ();

	}

	public void toInstructions()
	{
		SetUnactive ();
		instructionPanel.panelIn();
	}

	void SetUnactive()
	{
		if (gamePaused) 
		{
			transform.GetChild (0).gameObject.SetActive (false);
			gameManager.PauseGame ();
			gamePaused = false;
		}
	}

	public void exit()
	{
		Application.Quit();
	}
}
