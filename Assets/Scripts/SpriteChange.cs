﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChange : MonoBehaviour 
{

	public GameObject controller; // game object the sprite will overlay - ie the player or an enemy

	// control Rotation
	private Quaternion defaultRotation;

	//Player
	private GameObject player;
	private LanternController lanternController;
	private PlayerController playerController;

	//Enemy
	private EnemySight enemySight;

	// Sprites
	private Animator spriteAnimator;
	private Vector3 spriteOffset;

	// Light 
	public Animator lightAnimator;

	// Velocity
		// player
	//private Vector3 playerVelocity;
	public float playerVelocity;
	private Vector3 playerPreviousPosition;
		// enemy
	public float enemyVelocity;
	private Vector3 enemyPreviousPosition;

	void Start()
	{
		controller = this.transform.parent.gameObject;

		transform.rotation = Quaternion.Euler (90, 0, 0);
		defaultRotation = transform.rotation;

		//Player
		player = GameObject.FindWithTag("Player");
		lanternController = player.GetComponent<LanternController>();
		playerController = this.GetComponentInParent<PlayerController> ();

		//Enemy
		enemySight = this.gameObject.GetComponentInParent<EnemySight> ();

		spriteAnimator = this.gameObject.GetComponent<Animator> ();
		//spriteOffset = transform.position - controller.transform.position;

		if (this.gameObject.CompareTag ("PlayerIcon"))
			lightAnimator = player.transform.FindChild ("ViewVisuals").gameObject.GetComponent<Animator>();
		if (this.gameObject.CompareTag ("EnemyIcon")) 
			lightAnimator = this.transform.parent.FindChild("ViewVisuals").gameObject.GetComponent<Animator>();
	
	}

	void FixedUpdate()
	{
		if (GameManager.Instance.Paused)
			spriteAnimator.enabled = false;
		else {
			spriteAnimator.enabled = true;

			spriteAnimator.SetFloat ("Angle", controller.transform.eulerAngles.y); // set the angle in the animator
			lightAnimator.SetFloat ("Angle", controller.transform.eulerAngles.y);

			if (this.gameObject.CompareTag ("PlayerIcon")) 
			{
				spriteAnimator.SetBool ("UsingLantern", lanternController.lightSource.enabled);
				if (!lanternController.lightSource.enabled)
					this.lightAnimator.enabled = false;
				else
					this.lightAnimator.enabled = true;

				playerVelocity = ((playerController.transform.position - playerPreviousPosition) / Time.deltaTime).magnitude;
				playerPreviousPosition = playerController.transform.position;

				spriteAnimator.SetFloat ("Velocity", playerVelocity);
				lightAnimator.SetFloat ("Velocity", playerVelocity);
				/*
				float playerVelocity = Mathf.Abs (playerController.velocity.x) + 0f + Mathf.Abs(playerController.velocity.z);
				spriteAnimator.SetFloat ("Velocity", playerVelocity);
				lightAnimator.SetFloat ("Velocity", playerVelocity);
				*/
			}
			if (this.gameObject.CompareTag ("EnemyIcon")) 
			{

				//enemyVelocity = ((enemySight.transform.position - enemyPreviousPosition) / Time.deltaTime).magnitude;
				//enemyPreviousPosition = enemySight.transform.position;

				//spriteAnimator.SetFloat ("Velocity", enemyVelocity);
				//lightAnimator.SetFloat ("Velocity", enemyVelocity);

				enemyVelocity = Mathf.Abs (enemySight.velocity.x + 0f + enemySight.velocity.z);
				spriteAnimator.SetFloat ("Velocity", enemyVelocity);
				lightAnimator.SetFloat ("Velocity", enemyVelocity);
			}
		}
	}

	void LateUpdate () 
	{	
		if (GameManager.Instance.Paused) 
			spriteAnimator.enabled = false;
		else
		{
			spriteAnimator.enabled = true;

			// Roatation - or lack thereof 
			// So the sprite does not roatate with the controller
			if (transform.parent != null)
				transform.position = transform.parent.transform.position;
				//transform.position = new Vector3 (transform.parent.transform.position.x, 2f, transform.parent.transform.position.z);

			transform.rotation = defaultRotation;
			//transform.position = controller.transform.position + spriteOffset;

			/*

			spriteAnimator.SetFloat ("Angle", controller.transform.eulerAngles.y); // set the angle in the animator
			lightAnimator.SetFloat ("Angle", controller.transform.eulerAngles.y);

			if (this.gameObject.CompareTag ("PlayerIcon")) 
			{
				spriteAnimator.SetBool ("UsingLantern", lanternController.lightSource.enabled);
				if (!lanternController.lightSource.enabled)
					this.lightAnimator.enabled = false;
				else
					this.lightAnimator.enabled = true;

				playerVelocity = ((playerController.transform.position - playerPreviousPosition)/Time.deltaTime).magnitude;
				playerPreviousPosition = playerController.transform.position;

				spriteAnimator.SetFloat ("Velocity", playerVelocity);
				lightAnimator.SetFloat ("Velocity", playerVelocity);

				//float playerVelocity = Mathf.Abs (playerController.velocity.x) + 0f + Mathf.Abs(playerController.velocity.z);
				//spriteAnimator.SetFloat ("Velocity", playerVelocity);
				//lightAnimator.SetFloat ("Velocity", playerVelocity);
				
			}
			if (this.gameObject.CompareTag ("EnemyIcon")) 
			{
				
				enemyVelocity = ((enemySight.transform.position - enemyPreviousPosition)/Time.deltaTime).magnitude;
				enemyPreviousPosition = enemySight.transform.position;

				spriteAnimator.SetFloat ("Velocity", enemyVelocity);
				lightAnimator.SetFloat ("Velocity", enemyVelocity);

				
				//float enemyVelocity = Mathf.Abs (enemySight.velocity.x + 0f + enemySight.velocity.z);
				//spriteAnimator.SetFloat ("Velocity", enemyVelocity);
				//lightAnimator.SetFloat ("Velocity", enemyVelocity);

			}

		*/
		}
	}


}
