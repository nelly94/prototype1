﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EnterMazeDetector : MonoBehaviour 
{
	private GameObject player;
	private LanternController lanternController;

	//private DialogueManager dialogueManager;
	private DialogueDisplay dialogueDisplay;

	private InfoPanel infoPanel;
	public List<GameObject> oilResources;
	public List<GameObject> flintResources;
	public float oilTotal;
	public int flintTotal;

	//private InstructionsPanel instructionPanel;
	//private LoadPages loadpages;

	private TutorialPanel tutorialPanel;
	private LoosePages loosePage;


	void Awake()
	{
		player = GameObject.FindWithTag("Player");
		lanternController = player.GetComponent<LanternController>();

		//dialogueManager = FindObjectOfType<DialogueManager> ();
		dialogueDisplay = FindObjectOfType<DialogueDisplay> ();

		infoPanel = FindObjectOfType<InfoPanel> ();


		//instructionPanel = GameObject.FindWithTag ("InstructionPanel").GetComponent<InstructionsPanel> ();
		//loadpages = instructionPanel.gameObject.GetComponentInChildren<LoadPages> ();

		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();
		loosePage = tutorialPanel.gameObject.GetComponentInChildren<LoosePages> ();

	}

	void Start()
	{
		oilResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Oil"));
		flintResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Flint"));

		oilTotal = oilResources.Count;
		flintTotal = flintResources.Count;

		StartCoroutine(WaitPageOne(1f));
	}

	IEnumerator WaitPageOne(float time) 
	{
		yield return new WaitForSeconds(time);
		if (!loosePage.page1Seen) 
		{
			tutorialPanel.panelIn ();
			loosePage.LoosePageOne ();
			loosePage.page1Seen = true;
		}
	}

	void Update ()
	{
		// COLLECTION

		oilResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Oil"));
		flintResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Flint"));

		if ((flintTotal != flintResources.Count) || (oilTotal != oilResources.Count) || (lanternController.lantern == null))
		{	if (!infoPanel.hasBeenSeen)
			{
				infoPanel.panelIn ();
				infoPanel.hasBeenSeen = true;
			}
		}

		/*
		// Not sure if this is good or not
		// WSAD cancel page
		if (loosePage.loosePages [0].gameObject.activeSelf)
		{
			if ((Input.GetKeyDown (KeyCode.W)) || (Input.GetKeyDown (KeyCode.S)) || (Input.GetKeyDown (KeyCode.A)) || (Input.GetKeyDown (KeyCode.D))) 
			{
				loosePage.backToGame ();
			}
		}
		*/

		//if (Input.GetAxisRaw ("Horizontal") || Input.GetAxisRaw ("Vertical")) 
		if ((Input.GetKeyDown (KeyCode.W)) || (Input.GetKeyDown (KeyCode.S)) || (Input.GetKeyDown (KeyCode.A)) || (Input.GetKeyDown (KeyCode.D)))
		{
			if (!loosePage.page2Seen)
			{
				if (loosePage.page1Seen)
				{	
					if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
					{
						tutorialPanel.panelIn ();
						loosePage.LoosePageTwo ();
						loosePage.page2Seen = true;
					}
				}
			}
		}

		if (flintResources.Count == flintTotal - 1) 
		{
			if (!loosePage.page4Seen) 
			{
				if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
				{
					tutorialPanel.panelIn ();
					//tutorialPanel.tutorialPanelIn = true;
					loosePage.LoosePageThree ();

					loosePage.page4Seen = true;
				}
			}
		}
		if (!loosePage.page3Seen) 
		{
			if (oilResources.Count == oilTotal - 1) 
			{	
				if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
				{
					tutorialPanel.panelIn ();
					//tutorialPanel.tutorialPanelIn = true;
					loosePage.LoosePageFour ();
					loosePage.page3Seen = true;
				}
			}
		}	
	}
				

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player)
		{
			if (gameObject.CompareTag ("Detector")) 
			{
				//if ((oilTotal == oilResources.Count) && (flintTotal == flintResources.Count) & (lanternController.lantern != null))
				//	Debug.Log ("It is dark. I need some sort of light");
				if (lanternController.lantern != null) 
					dialogueDisplay.ShowBox("It is dark. I need some sort of light.");
			
				if (lanternController.lantern == null) 
				{	

					if ((lanternController.oilAmount > 0) && (lanternController.flintAmount == 0)) 
						dialogueDisplay.ShowBox("I need some flint");
			
					if ((lanternController.oilAmount == 0) && (lanternController.flintAmount > 0)) 
					{
						dialogueDisplay.ShowBox("I need some oil");
					}
					if ((lanternController.oilAmount == 0) && (lanternController.flintAmount == 0)) 
					{
						dialogueDisplay.ShowBox("I need to find some oil and flint");
					} 
				} 
			}		
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject == player) 
		{
			if (gameObject.CompareTag ("Detector")) 
			{
				if ((lanternController.lantern == null) && (lanternController.oilAmount > 0) && (lanternController.flintAmount > 0)) 
				{
					if (!lanternController.lightSource.enabled) 
					{
						//dialogueDisplay.ShowBox ("Mouse Left click to activate the light");
						if (!loosePage.page5Seen)
						{	
							if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
							{
								tutorialPanel.panelIn ();
								loosePage.LoosePageFive ();
								loosePage.page5Seen = true;
							}
						}

						if (loosePage.page5Seen) 
						{
							if (!loosePage.loosePages [4].gameObject.activeSelf) 
							{
								StartCoroutine(WaitLight(3f));
							}
						}




						if ((Input.GetMouseButtonDown (0))) 
						{
							//if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
								dialogueDisplay.ShowBox ("Use the mouse wheel or up-down arrows to control the light's range");
						}
						if ((Input.GetAxis ("Mouse ScrollWheel")) < 0f || Input.GetKeyDown (KeyCode.DownArrow))
							Debug.Log ("mouse wheel");//Destroy (gameObject);
					} 
					else
						Destroy (gameObject);
				}
			}
		}
	}

	IEnumerator WaitLight(float time) 
	{
		yield return new WaitForSeconds(time);

		if (!lanternController.lightSource.enabled) 
			dialogueDisplay.ShowBox ("Left click on the mouse to activate the light");
	}

}

/*
void Awake()
	{
		player = GameObject.FindWithTag("Player");
		lanternController = player.GetComponent<LanternController>();

		//dialogueManager = FindObjectOfType<DialogueManager> ();
		dialogueDisplay = FindObjectOfType<DialogueDisplay> ();
	
		
		oilResources = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Oil"));
		flintResources = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Flint"));
	}

	void Start()
	{
		oilTotal = oilResources.Count;
		flintTotal = flintResources.Count;
	}
	void Update ()
	{
		// COLLECTION
		oilResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Oil"));
		flintResources = new List<GameObject> (GameObject.FindGameObjectsWithTag ("Flint"));
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player)
		{
			if (gameObject.CompareTag ("Detector")) 
			{
				//if ((oilTotal == oilResources.Count) && (flintTotal == flintResources.Count) & (lanternController.lantern != null))
				//	Debug.Log ("It is dark. I need some sort of light");
				if (lanternController.lantern != null) 
					dialogueDisplay.ShowBox("It is dark. I need some sort of light.");
			
				if (lanternController.lantern == null) 
				{	

					if ((oilTotal != oilResources.Count) && (flintTotal == flintResources.Count)) 
						dialogueDisplay.ShowBox("I need some flint");
			
					if ((oilTotal == oilResources.Count) && (flintTotal != flintResources.Count)) 
					{
						dialogueDisplay.ShowBox("I need some oil");
					}
					if ((oilTotal == oilResources.Count) && (flintTotal == flintResources.Count)) 
					{
						dialogueDisplay.ShowBox("I need to find some oil and flint");
					} 
				} 
			}		
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject == player) 
		{
			if (gameObject.CompareTag ("Detector")) 
			{
				if ((lanternController.lantern == null) && (oilTotal != oilResources.Count) && (flintTotal != flintResources.Count)) 
				{
					if (!lanternController.lightSource.enabled) 
					{
						dialogueDisplay.ShowBox ("Mouse Left click to activate the light");
						if (Input.GetMouseButtonDown (0))
							dialogueDisplay.ShowBox ("Use the mouse wheel to control the light's range");
						if ((Input.GetAxis ("Mouse ScrollWheel")) < 0f || Input.GetKeyDown (KeyCode.DownArrow))
							Destroy (gameObject);
					} 
					else
						Destroy (gameObject);
				}
			}
		}
	}

*/
