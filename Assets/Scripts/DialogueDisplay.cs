﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DialogueDisplay : MonoBehaviour 
{

	public GameObject dialogueBox;
	//public GameObject dialogueBackground;
	private GameObject dispaly;

	public Image dialogueImage;
	public Sprite imageSmall;
	public Sprite imageBigger;

	public GameObject smallBox;
	public GameObject bigBox;


	public Text dialogueText;
	public bool dialogueActive = false;
	public float dialogueWaitTimer = 5.0f;
	public float dialogueTimer;

	private InfoPanel infoPanel;
	private TutorialPanel tutorialPanel;

	void Awake()
	{
		dialogueBox = this.gameObject;
		dialogueImage = dialogueBox.transform.GetChild(0).GetComponentInChildren<Image> ();
		Debug.Log (dialogueImage);
		dispaly = dialogueBox.transform.GetChild (0).gameObject;

		//dialogueBackground.SetActive (false);
		dialogueActive = false;
		dispaly.SetActive (false);
		dialogueTimer = 0f;

		infoPanel = FindObjectOfType<InfoPanel> ();
		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();

	}

	void Update()
	{
		if (infoPanel.PanelIn) 
			dialogueText = smallBox.GetComponent<Text> ();
		else
			dialogueText = bigBox.GetComponent<Text> ();

		if (!tutorialPanel.tutorialPanelIn) 
		{
			if (dialogueActive) 
			{
				if (infoPanel.PanelIn) 
				{
					//dialogueText = smallBox.GetComponent<Text> ();
					bigBox.SetActive (false);
					smallBox.SetActive (true);
					dialogueImage.sprite = imageSmall;
					//dialogueBackground.SetActive (true);
				}
				if (!infoPanel.PanelIn) 
				{
					//dialogueText = bigBox.GetComponent<Text> ();
					bigBox.SetActive (true);
					smallBox.SetActive (false);
					dialogueImage.sprite = imageBigger;
					//dialogueBackground.SetActive (false);
				}
				dialogueTimer += Time.deltaTime;
				if ((dialogueWaitTimer <= dialogueTimer)) 
				{
					//dialogueActive = false;
					//dispaly.SetActive (false);
					//dialogueBackground.SetActive (false);
					//dialogueTimer = 0f;
					HideText();
					//Debug.Log ("turn off");
				}
			}
		} 
		else 
		{
			HideText ();
		}
	}

	public void HideText()
	{
		dialogueActive = false;
		dispaly.SetActive (false);
		//dialogueBackground.SetActive (false);
		dialogueTimer = 0f;
	}

	public void ShowBox(string dialogue)
	{	
		/*
		if (infoPanel.PanelIn) 
		{
			dialogueImage.sprite = imageSmall;
			dialogueBackground.SetActive (true);
		}
		if (!infoPanel.PanelIn)
		{
			dialogueImage.sprite = imageBigger;
			dialogueBackground.SetActive (false);
		}
		*/
		if (!tutorialPanel.tutorialPanelIn) 
		{
			dialogueActive = true;
			dispaly.SetActive (true);
			dialogueText.text = dialogue;
		}
		//Debug.Log ("turn on");	
	}
}
