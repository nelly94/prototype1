﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class LightManager : MonoBehaviour 
{

	private CursorHandler cursorHandler;
	private LightDetectionManager lightDetection;
	private GameObject player;
	//private LanternController lanternController;

	Ray ray;
	RaycastHit hit;

	void Awake()
	{
		player = GameObject.FindWithTag("Player");		 
		cursorHandler = GameObject.FindWithTag ("CursorHandler").GetComponent<CursorHandler> ();
		//cursorHandler = FindObjectOfType<CursorHandler> ();
	}

	void Update ()
	{
		// https://docs.unity3d.com/Manual/Layers.html
		// Bit shift the index of the layer ("Selectable") to get a bit mask
		// This would cast rays only against colliders in layer "Selectable" .
		int layerMask = (1 << LayerMask.NameToLayer ("Selectable"));
		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (!GameManager.Instance.Paused) 
		{
			if (Physics.Raycast (ray, out hit, 100, layerMask)) 
			{
				//Cursor.SetCursor (cursor, Vector2.zero, CursorMode.Auto);
				//Debug.Log (hit.collider.name); 

				// PLAYER
				if (hit.collider.name == "PlayerController" || hit.collider.name == "Player" || hit.collider.name == "PlayerIcon") 
				{
					Cursor.SetCursor (cursorHandler.cursorHand, Vector2.zero, CursorMode.Auto);

					if (Input.GetMouseButtonDown (0))
						player.SendMessageUpwards("PlayerLightOn");
						//hit.collider.gameObject.SendMessage("PlayerLightOn");
					if (Input.GetMouseButtonDown (1))
						player.SendMessageUpwards("PlayerLightOff");
						//hit.collider.gameObject.SendMessage("PlayerLightOff");
				} 
				else 
				{
					// LIGHTS IN SCENE
					lightDetection = hit.collider.gameObject.GetComponentInParent<LightDetectionManager> ();
		
					if (lightDetection.isInReach) 
					{
						Cursor.SetCursor (cursorHandler.cursorHand, Vector2.zero, CursorMode.Auto);
						if (!lightDetection.light.activeSelf)
							lightDetection.SendMessageUpwards ("activateFade");
						if (Input.GetMouseButtonDown (0))
							lightDetection.SendMessageUpwards ("TurnLightOn");
						if (Input.GetMouseButtonDown (1))
							lightDetection.SendMessageUpwards ("TurnLightOff");
					} 
				}
				
			} 
			else 
			{
				Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
				//Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
				//Debug.Log ("not over an object");
			}
		}
	}

}

/*
	public Texture2D cursor;
	private LightDetectionManager lightDetection;
	private LanternController lanternController;


	void Awake()
	{
		lightDetection = GetComponentInParent<LightDetectionManager>();
		lanternController = FindObjectOfType<LanternController> ();
	}

	void OnMouseOver()
	{
		if (!EventSystem.current.IsPointerOverGameObject ()) 
		{
			Debug.Log ("I'm over an object"); 
			if (lightDetection.isInReach) 
			{
				Cursor.SetCursor (cursor, Vector2.zero, CursorMode.Auto);
				if ((!lightDetection.rendererMesh.enabled) && (!lightDetection.light.enabled)) 
				{
					if (Input.GetKeyDown (KeyCode.E) && (lanternController.flintAmount > 0)) 
					{
						lanternController.StrikeFlint ();
						Debug.Log ("turn the light on");
						lightDetection.rendererMesh.enabled = true;
						lightDetection.light.enabled = true;
					}
				} else {
					if (Input.GetKeyDown (KeyCode.E)) {
						Debug.Log ("turn the light off");
						lightDetection.rendererMesh.enabled = false;
						lightDetection.light.enabled = false;
					}				
				}
			} 
			else
				Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
		}
	}

*/