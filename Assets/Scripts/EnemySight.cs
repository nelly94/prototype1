﻿// ____ REFERENCES ____ 
// adapted from Renaissance Coders's "Unity3D Artificial Intelligence in C#" tutorials from https://www.youtube.com/watch?v=7TVXCa2Nwj4&t=628s ( playlist https://www.youtube.com/watch?v=PYzPLeeXd04&list=PL4CCSwmU04Mh8GHV702HxgfRpYmgTwq2l)
// adapted from Unity's "Stealth game" tutorials from https://www.youtube.com/watch?v=mBGUY7EUxXQ&t=229s ( playlist https://www.youtube.com/watch?v=_TAY-U9eACg&list=PLX2vGYjWbI0QGyfO8PKY1pC8xcRb0X-nP)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySight : MonoBehaviour 
{
	public Vector3 velocity;
	public bool isPaused;
	//public float fieldOfView = 110f; 									// number of degrees in the forward direction that enemy can see through
	//public bool playerInSight;										// sets to true when enemy can see the player
	//public GameObject visibility;
	public Vector3 personalLastSighting;								// Last place this enemy spotted the player.
	public Vector3 position = new Vector3(1000f, 1000f, 1000f);			// The last global sighting of the player.
	public Vector3 resetPosition = new Vector3(1000f, 1000f, 1000f);	// The default position if the player is not in sight.
	//public Vector3 position = new Vector3(000f, 000f, 000f);			// The last global sighting of the player.
	//public Vector3 resetPosition = new Vector3(1f, 1f,1f);	// The default position if the player is not in sight.

	//public float speed = 4.8f;
	public float minDistance = 1f; 

	public Transform[] wayPoints;
	public float patrolSpeed;
	public float chaseSpeed;
	public float chaseWaitTime = 5f;
	public float patrolWaitTime = 5f;

	public enum Behaviour
	{
		Patrol,
		Chase
	};

	public Behaviour behaviour;

	private int wayPointIndex;
	//private GameObject player;			// Reference to the player.
	private NavMeshAgent agent;			// Reference to the NavMeshAgent component.
	//private SphereCollider spherecol;	// Reference to the sphere collider trigger component.
	private Vector3 previousSighting;	// Where the player was sighted last frame.
	public float chaseTimer;			// A timer for the chaseWaitTime.
	public float patrolTimer;			// A timer for the patrolWaitTime.
	private bool alive;
	//private FieldOfView fieldvision;

	private GameObject player;

	void Awake()
	{
		//player = GameObject.FindWithTag("Player");
		//fieldvision = GetComponent<FieldOfView> ();

		agent = gameObject.GetComponentInParent<NavMeshAgent> ();

		//spherecol = GetComponent<SphereCollider> ();

		behaviour = EnemySight.Behaviour.Patrol;
		alive = true;
		StartCoroutine(finiteStateMachine());

		// Set the personal sighting and the previous sighting to the reset position.
		personalLastSighting = resetPosition;
		previousSighting = resetPosition;

		//wayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

		player = GameObject.FindGameObjectWithTag ("PlayerController");
	}

	void Start()
	{
		//wayPointIndex = Random.Range (0, wayPoints.Length); // set starting value from objects in array		

	}

	void Update()
	//void FixedUpdate ()
	{	
			velocity = agent.velocity;		
			//{

			// run the following when not paused


			// If the last global sighting of the player has changed...
			if (position != previousSighting)
				// ... then update the personal sighting to be the same as the global sighting.
				personalLastSighting = position;

			// Set the previous sighting to the be the sighting from this frame.
			previousSighting = position;

			//if (playerInSight == true)
			//behaviour = EnemySight.Behaviour.Chase;
			//else if (playerInSight == false)
			//behaviour = EnemySight.Behaviour.Patrol;
				
			if (personalLastSighting != resetPosition)
				behaviour = EnemySight.Behaviour.Chase;
			else
				behaviour = EnemySight.Behaviour.Patrol;
		//}
	}

	IEnumerator finiteStateMachine()
	{

		while (alive) 
		{
			switch (behaviour) 
			{
				case Behaviour.Patrol:
					Patrol ();
								//Debug.Log ("patrol state");
					break;
				case Behaviour.Chase:
					Chase ();
								//Debug.Log ("chase state");
					break;
			}
			yield return null;
		}
		//}
	}

	void Patrol()
	{
		if (!GameManager.Instance.Paused) 
		{
			agent.enabled = true;
			/*
				// set the speed for the NavMeshAgent.
				if (GameManager.Instance.Paused)
					agent.speed = 0f;
				else
					agent.speed = patrolSpeed;
			*/

			// Set the destination to the patrolWayPoint.
			agent.destination = wayPoints [wayPointIndex].position;

			// If near the next waypoint or there is no destination...
			//if(agent.destination == resetPosition || agent.remainingDistance < agent.stoppingDistance)
			if (agent.remainingDistance <= agent.stoppingDistance) 
			{
				// increment timer.
				patrolTimer += Time.deltaTime;

				// If the timer exceeds the wait time...
				if (patrolTimer >= patrolWaitTime) 
				{
					//  increment the wayPointIndex.
					//if(wayPointIndex == wayPoints.Length - 1)
					//wayPointIndex = 0;
					//else
					//wayPointIndex++;
					wayPointIndex = Random.Range (0, wayPoints.Length);

					// Reset the timer.
					patrolTimer = 0;
				}
			} 
			else
					// If not near a destination, reset the timer.
					patrolTimer = 0;

			/*

				if (Vector3.Distance (this.transform.position, wayPoints [wayPointIndex].transform.position) >= 2)
					agent.SetDestination (wayPoints [wayPointIndex].transform.position);
				else if (Vector3.Distance (this.transform.position, wayPoints [wayPointIndex].transform.position) <= 2)
					//wayPointIndex = (wayPointIndex + 1) % wayPoints.Length;
					wayPointIndex = Random.Range (0, wayPoints.Length);

				*/


			/*
				if (agent.remainingDistance < 0.5f)
					agent.SetDestination (wayPoints [wayPointIndex].transform.position);
				else
					wayPointIndex = (wayPointIndex + 1) % wayPoints.Length;
				*/
			//}
		} 
		else
			agent.enabled = false;
	}

	void Chase()
	{
		if (!GameManager.Instance.Paused) 
		{
			agent.enabled = true;
			//Debug.Log ("ChaseWorking");
			// set the speed for the NavMeshAgent.
			/*
			if (GameManager.Instance.Paused)
				agent.speed = 0f;
			else
				agent.speed = chaseSpeed;
		*/

			// Create a vector from the enemy to the last sighting of the player.
			Vector3 sightingDeltaPos = personalLastSighting - transform.position;

			//Debug.Log (sightingDeltaPos.sqrMagnitude);
			// If the the last personal sighting of the player is not close
			if (sightingDeltaPos.sqrMagnitude > 4f) 
			{
				// set the destination for the NavMeshAgent to the last personal sighting of the player.
				agent.destination = personalLastSighting; 
				//Debug.Log("ifsightdelta");
			}

			// If near the last personal sighting
			if (agent.remainingDistance <= agent.stoppingDistance) 
			{
				//Debug.Log("ifnearlastsighting");
				// increment timer.
				chaseTimer += Time.deltaTime;

				// If the timer exceeds waiting time
				if (chaseTimer >= chaseWaitTime) 
				{
					// reset last global sighting, the last personal sighting and the timer.
					//Debug.Log("timerchase");

					position = resetPosition;
					personalLastSighting = resetPosition;
					chaseTimer = 0f;
				}
			}
			else
				// If not near the last sighting personal sighting of the player, reset the timer.
				chaseTimer = 0f;

			//}
		}
		else
			agent.enabled = false;
	}
}

	/*
	void OnTriggerStay(Collider other)
	{
		//if(other.gameObject == player) 
		if(other.gameObject == player)
		{
			playerInSight = false;
			behaviour = EnemySight.Behaviour.Patrol;

			// Create a vector from the enemy to the player and store the angle between it and forward.
			Vector3 direction = other.transform.position - transform.position;
			float angle = Vector3.Angle (direction, transform.forward);

			Debug.DrawRay(transform.position, direction, Color.green);

			// If the angle between forward and where the player is, is less than half the angle of view.
			if(angle < fieldOfView * 0.5f) 
			{
				RaycastHit hit;

				// raycast cast towards the player hits something	(direction of a raycast is always normalised to always have a magnitude of one)
				if(Physics.Raycast(transform.position, direction.normalized, out hit, spherecol.radius))
				{
					//if(hit.collider.gameObject == player) 
					if(hit.collider.gameObject == player) 
					{
						playerInSight = true;
						behaviour = EnemySight.Behaviour.Chase;
						transform.LookAt (player.transform);	

						// Set the last global sighting is the players current position.
						position = player.transform.position;


						//float distance = Vector3.Distance(player.transform.position, transform.position);	
						//if (distance > minDistance)
						//{ // check min distance
						//		// only move to the target if farther than min distance
						//		transform.position += transform.forward * speed * Time.deltaTime;
						//}

					
					}						
				}					
			}
		}
	}



	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
			playerInSight = false;
	}
*/
