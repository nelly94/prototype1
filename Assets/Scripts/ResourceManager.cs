﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour 
{
	public List<Transform> ResourceSpawn = new List<Transform>();

	//public Transform[] ResourceSpawn;
	public int ResourceSpawnIndex;

	public GameObject oil;
	public GameObject flint;

	public int oilInstancesAllowed = 2;
	public int flintInstancesAllowed = 2;
	public int oilInstances = 0;
	public int flintInstances = 0;

	//private int chooseBetween; 


	//private ResourceCheck resourceCheck;

	void Awake ()
	{
		//resourceCheck = GetComponentInChildren<Resources>;
		//foreach (GameObject child in gameObject)
		foreach (Transform child in transform)
			if (child.CompareTag ("ResourceSpawn")) 
			{
				child.GetComponent<ResourceCheck> ().isFilled = false;
				ResourceSpawn.Add (child);				
			}

		// spawning resources
		// this is still not working!

		while (oilInstances < oilInstancesAllowed) 
		{
				ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);

			if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == false) 
			{
					Instantiate (oil, ResourceSpawn [ResourceSpawnIndex].transform.position, ResourceSpawn [ResourceSpawnIndex].transform.rotation, ResourceSpawn [ResourceSpawnIndex]);
					ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled = true;
					oilInstances++;
			}
		}
		while (flintInstances < flintInstancesAllowed) 
		{
			ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);

			if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == false) 
			{
				Instantiate (flint, ResourceSpawn [ResourceSpawnIndex].transform.position, ResourceSpawn [ResourceSpawnIndex].transform.rotation,ResourceSpawn [ResourceSpawnIndex]);
				ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled = true;
				flintInstances++;
				
			}
		}
	}
}

/*

 * while ((oilInstances < oilInstancesAllowed) && (flintInstances < flintInstancesAllowed)) 
		{
			if ((oilInstances < oilInstancesAllowed) && (flintInstances == flintInstancesAllowed))
				chooseBetween = 2;
			else if ((oilInstances == oilInstancesAllowed) && (flintInstances < flintInstancesAllowed))
				chooseBetween = 1;
			else
				chooseBetween = Random.Range (1, 3); 
		}

			if (chooseBetween == 1) 
			{
				ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);
				if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == false) {
					Instantiate (oil, ResourceSpawn [ResourceSpawnIndex].transform.position, ResourceSpawn [ResourceSpawnIndex].transform.rotation);
					ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled = true;
					oilInstances++;
				} 
				else if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == true) 
				{
					while (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == true) 
					{
						ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);
					}
				}
			}
			if (chooseBetween == 2)
			{
				ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);
				if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == false) {
					Instantiate (flint, ResourceSpawn [ResourceSpawnIndex].transform.position, ResourceSpawn [ResourceSpawnIndex].transform.rotation);
					ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled = true;
					flintInstances++;
				} 
				else if (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == true) 
				{
					while (ResourceSpawn [ResourceSpawnIndex].GetComponent<ResourceCheck> ().isFilled == true) 
					{
						ResourceSpawnIndex = Random.Range (0, ResourceSpawn.Count);
					}
				}
			}
	}


*/
