﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPanel : MonoBehaviour 
{

	private GameManager gameManager;
	private InstructionsPanel instructionPanel;
	private MenuPanel menuPanel;

	//public bool gamePaused = false;
	private LoosePages loosePage;

	public GameObject loosePageManager;

	public bool tutorialPanelIn;


	//public bool tutorialPanelIn;

	void Start ()
	{
		//gameManager = FindObjectOfType<GameManager> (); 
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		instructionPanel = GameObject.FindGameObjectWithTag ("InstructionPanel").GetComponent<InstructionsPanel> ();
		menuPanel = GameObject.FindGameObjectWithTag("EscMenu").GetComponent<MenuPanel>();

		loosePage = this.gameObject.GetComponentInChildren<LoosePages> ();
		loosePage.gameObject.transform.GetChild(0).gameObject.SetActive (false);

		loosePageManager = loosePage.gameObject.transform.GetChild (0).gameObject;
	}
		
	void Update ()
	{
		if (!instructionPanel.helpPanelIn && (!menuPanel.menuPanelIn))
			tutorialPanelIn = (loosePageManager.activeSelf) ? true : false; 
	}

	public void panelIn()
	{
		
		gameManager.PauseGame ();
		loosePage.enabled = true;
		//loosePage.gameObject.transform.GetChild(0).gameObject.SetActive (true);
		loosePageManager.SetActive (true);
	}

	public void panelOut()
	{
		gameManager.PauseGame ();
		loosePage.enabled = false;
		//loosePage.gameObject.transform.GetChild(0).gameObject.SetActive (false);
		loosePageManager.SetActive (false);

		for (int index = 0; index < loosePage.loosePages.Length; index++) 
		{
			loosePage.loosePages [index].gameObject.SetActive (false);
		}
	}




}

/*

	public void Update () 
	{

		tutorialPanelIn = (loosePage.gameObject.transform.GetChild(0).gameObject.activeSelf) ? true : false; 

		if (tutorialPanelIn)
			panelIn ();
		else if (!tutorialPanelIn)
			panelOut();

	}


	public void panelIn()
	{
		loosePage.gameObject.transform.GetChild(0).gameObject.SetActive(true);
	}

	public void panelOut()
	{
		loosePage.gameObject.transform.GetChild(0).gameObject.SetActive (false); 


			//for (int index = 0; index < loosePage.loosePages.Length; index++) 
			//{
				//loosePage.loosePages [index].gameObject.SetActive (false);
	}


 */