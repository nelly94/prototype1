﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
	public GameObject player;

	// offset value between the player and the camera
	private Vector3 cameraOffset;

	void Awake () 
	{
		cameraOffset = transform.position - player.transform.position;
	}
	
	// "LateUpdate" like "Update" runs every frame, however is guaranteed to run after all items have been processed in update
	// ie it is guarenteed that the player has moved for that frame before the camera moves
	void LateUpdate () 
	{
		transform.position = player.transform.position + cameraOffset;
	}
}
