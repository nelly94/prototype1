﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISoundManager : MonoBehaviour 
{

	public AudioSource audioSource;
	public AudioClip[] woodSounds;
	public AudioClip[] paperTapSounds;
	public AudioClip[] paperTurnSounds;
	public AudioClip[] paperPickUp;
	public AudioClip touchingPaperSound;

	public float volume;

	// Use this for initialization
	void Awake ()
	{
		audioSource = this.gameObject.GetComponent<AudioSource> ();
	
		woodSounds = Resources.LoadAll<AudioClip> ("Sounds/Wood/WoodKnocks");
		paperTapSounds = Resources.LoadAll<AudioClip> ("Sounds/Paper/PaperTaps");
		paperTurnSounds = Resources.LoadAll<AudioClip> ("Sounds/Paper/TurnPage");
		touchingPaperSound = Resources.Load<AudioClip> ("Sounds/Paper/MovingOnPaper");
		paperPickUp = Resources.LoadAll<AudioClip> ("Sounds/Paper/PagePickUp");
	}	

	public void paperTurnSound()
	{
		volume = Random.Range(0.6f, 0.95f);
		audioSource.clip = paperTurnSounds[Random.Range(0, paperTurnSounds.Length-1)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

	public void paperTapSound()
	{
		volume = Random.Range(0.6f, 0.95f);
		audioSource.clip = paperTapSounds[Random.Range(0,paperTapSounds.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

	public void fingerOnPaper()
	{
		volume = 0.6f;
		audioSource.clip = touchingPaperSound;
		audioSource.Play ();
		audioSource.loop = true;
	}

	public void woodSound()
	{
		volume = Random.Range(0.6f, 0.95f);
		audioSource.clip = woodSounds[Random.Range(0,woodSounds.Length)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}
	public void paperPickUpSound()
	{
		volume = Random.Range(0.6f, 0.95f);
		audioSource.clip = paperPickUp[Random.Range(0, paperPickUp.Length-1)];
		audioSource.PlayOneShot (audioSource.clip, volume);
	}

}
