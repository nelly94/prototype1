﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuDontDestroy : MonoBehaviour 
{
	private static MenuDontDestroy instanceReference;
	
	private SoundFade soundFade;
	private CameraShift cameraShift;

	private string sceneName;

	void Awake()
	{
		sceneName = SceneManager.GetActiveScene ().name;

		if (sceneName == "menuattemp1")
		{
			if (instanceReference == null) 
		{
				instanceReference = this;
			DontDestroyOnLoad (gameObject);
		} 
		else
			DestroyImmediate (gameObject);
		}

		/*
		if (sceneName == "menuattemp1")
		{
			if (!instanceReference) 
				instanceReference = this;
			else
				Destroy(this.gameObject);

			DontDestroyOnLoad(this.gameObject);
		}
		*/
	}
}
