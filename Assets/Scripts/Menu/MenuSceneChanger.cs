﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuSceneChanger : MonoBehaviour 
{
	private ButtonController buttonController;

	public GameObject loadingScreen;
	public GameObject playButton;
	public GameObject helpButton;
	public GameObject exitButton;

	private FadeScreen fadeScreen;

	private UISoundManager uiSoundManager;

	void Awake()
	{
		buttonController = FindObjectOfType<ButtonController> ();

		loadingScreen = GameObject.FindGameObjectWithTag ("LoadingScreen").transform.FindChild ("Black").gameObject;
		playButton = GameObject.FindGameObjectWithTag ("PlayButton");
		helpButton = GameObject.FindGameObjectWithTag ("HelpButton");
		exitButton = GameObject.FindGameObjectWithTag ("ExitButton");
		fadeScreen = FindObjectOfType<FadeScreen> ();

		uiSoundManager = FindObjectOfType<UISoundManager> ();
	}

	public IEnumerator ChangeScenesPlay()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		loadingScreen.SetActive (true);
		StartCoroutine (LoadGame ());
	}

	private IEnumerator LoadGame()
	{

		AsyncOperation operation = SceneManager.LoadSceneAsync ("09-03");

		while (!operation.isDone) 
		{


			yield return null;
		}
	}

	public IEnumerator ChangeScenesHelp()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		SceneManager.LoadScene ("Help2");
	}

	public IEnumerator ChangeScenesExit()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		Application.Quit ();
	}

}
