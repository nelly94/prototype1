﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CameraShift : MonoBehaviour 
{
	//private static CameraShift instanceReferenceCam;

	public Animator animator;
	public Animation animation;
	//public bool shifttoMenu;
	public float MenuTime;
	public float MenuTimer = 0f;

	private SoundFade soundFade;

	private string sceneName;

	void Awake () 
	{
		sceneName = SceneManager.GetActiveScene ().name;

		animator = this.gameObject.GetComponent<Animator>();
		soundFade = FindObjectOfType<SoundFade> ();

		//if (sceneName == "menuattemp1")
		//{
			//if (instanceReferenceCam == null) 
			//{
				//instanceReferenceCam = this;
				//DontDestroyOnLoad (gameObject);
			//} 
			//else
				//DestroyImmediate (gameObject);

		//}

	}

	void Update () 
	{
		if (!soundFade.cameraMoved) 
		{
			MenuTimer += Time.deltaTime;

			if (MenuTimer >= MenuTime) 
			{
				//shifttoMenu = true;
				animator.enabled = true;
				animator.SetBool ("shiftToMenu", true);
				animator.Play ("cameramove");
				while (animation.IsPlaying("cameramove"))
				{
					soundFade.cameraMoved = false;
				}
				soundFade.cameraMoved = true;


			}
		}			
	}
}
