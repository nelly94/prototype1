﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SoundFade : MonoBehaviour 
{
	public static SoundFade instanceReference;
	private CameraShift cameraShift;

	public bool cameraMoved;

	public GameObject nightSound;
	public GameObject music;

	private string sceneName;

	void Awake()
	{
		cameraShift = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraShift> ();

		sceneName = SceneManager.GetActiveScene ().name;

	}

	void Update()
	{
		sceneName = SceneManager.GetActiveScene ().name;
		Debug.Log ("running");
		if (sceneName == "menuattemp1") 
		{
			cameraShift.gameObject.SetActive (true);
			if (!cameraMoved)
			{
				if (cameraShift.MenuTimer >= (cameraShift.MenuTime - 2))
				music.SetActive (true);
			}
			else 
			{
				music.SetActive (true);
				nightSound.SetActive (true);
				music.GetComponent<AudioSource> ().spatialBlend = 1f;
				nightSound.GetComponent<AudioSource> ().spatialBlend = 1f;

			}
		}
		if ( sceneName == "09-03") 
		{
			cameraShift.animator.enabled = false;
			cameraShift.gameObject.SetActive (false);
			nightSound.SetActive (false);
			music.SetActive (false);
		}
		if (sceneName == "Help2")
			Make2DSound ();
		if (sceneName == "GameOver")
			Make2DSound ();
		if (sceneName == "Victory")
			Make2DSound ();	

	}

	public void Make2DSound()
	{
		cameraShift.animator.enabled = false;
		cameraShift.gameObject.SetActive (false);
		nightSound.SetActive (false);
		music.SetActive (true);
		music.GetComponent<AudioSource> ().spatialBlend = 0f;
	}
}



/*
 * var audio1Volume : float = 1.0;
 var audio2Volume : float = 0.0;
 var track2Playing : boolean = false;
 
 function Update() {
     fadeOut();
 
     if (audio1Volume <= 0.1) {
         if(track2Playing == false)
         {
           track2Playing = true;
           audio.clip = track2;
           audio.Play();
         }
         
         fadeIn();
     }
 }
 
 function OnGUI()
 {
     GUI.Label(new Rect(10, 10, 200, 100), "Audio 1 : " + audio1Volume.ToString());
     GUI.Label(new Rect(10, 30, 200, 100), "Audio 2 : " + audio2Volume.ToString());
 }
 
 function fadeIn() {
     if (audio2Volume < 1) {
         audio2Volume += 0.1 * Time.deltaTime;
         audio.volume = audio2Volume;
     }
 }
 
 function fadeOut() {
     if(audio1Volume > 0.1)
     {
         audio1Volume -= 0.1 * Time.deltaTime;
         audio.volume = audio1Volume;
     }
 }
 */
