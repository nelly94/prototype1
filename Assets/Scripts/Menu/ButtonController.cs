﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonController : MonoBehaviour 
{
	private MenuSceneChanger menuScene;

	private CursorHandler cursorHandler;
//	private SpriteRenderer spriteRender;
	public GameObject loadingScreen;

	private GameObject sprite;
	private FadeScreen fadeScreen;
	//public string sceneName;
	public bool exitGame = false;

	private UISoundManager uiSoundManager;

	void Awake()
	{
		menuScene = FindObjectOfType<MenuSceneChanger> ();
		//sprite = this.gameObject.GetComponentInChildren<SpriteRenderer> ();
		sprite = this.gameObject.GetComponentInChildren<SpriteRenderer> ().gameObject;
		loadingScreen = GameObject.FindGameObjectWithTag ("LoadingScreen").transform.FindChild ("Black").gameObject;
		fadeScreen = FindObjectOfType<FadeScreen> ();

		//cursorHandler = FindObjectOfType<CursorHandler> ();
		cursorHandler = GameObject.FindWithTag ("CursorHandler").GetComponent<CursorHandler> ();
		uiSoundManager = FindObjectOfType<UISoundManager> ();
	}

void Start () 
	{
		sprite.SetActive(false);
	}

	void OnMouseOver()
	{
		sprite.SetActive (true);
		Cursor.SetCursor (cursorHandler.cursorHand, Vector2.zero, CursorMode.Auto);

		if ((this.gameObject.CompareTag ("PlayButton") && Input.GetMouseButtonDown (0))) 
		{
			//sceneName = "09-03";
			StartCoroutine (menuScene.ChangeScenesPlay());

		}
			

		if ((this.gameObject.CompareTag ("HelpButton") && Input.GetMouseButtonDown (0))) 
		{
			//sceneName = "Help2";
			StartCoroutine (menuScene.ChangeScenesHelp());

		}

		if ((this.gameObject.CompareTag ("ExitButton") && Input.GetMouseButtonDown (0))) 
		{
			//exitGame = true;
			StartCoroutine (menuScene.ChangeScenesExit());
		}
	}

	void OnMouseEnter()
	{
		uiSoundManager.woodSound ();
	}

	void OnMouseExit()
	{
		sprite.SetActive (false);
		Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
	}

}