﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceCollection : MonoBehaviour 
{
	public int flintStrike;
	public float oilPickedUp;

	private GameObject player;
	private PlayerSounds playerSounds;
	private LanternController lanternController;
	private EnterMazeDetector enterMazeDetector;
	private DialogueDisplay dialogueDisplay;
	private ResourceCheck resourceCheck;
	private InfoPanel infoPanel;
	//private DialogueManager dialogueManager;

	private int i = 0;

	void Awake()
	{
		player = GameObject.FindWithTag("Player");
		playerSounds = player.GetComponent<PlayerSounds> ();
		lanternController = player.GetComponent<LanternController>();

		//dialogueManager = FindObjectOfType<DialogueManager> ();
		enterMazeDetector = FindObjectOfType<EnterMazeDetector> ();
		dialogueDisplay = FindObjectOfType<DialogueDisplay> ();
		resourceCheck = GetComponentInParent<ResourceCheck> ();
		infoPanel = infoPanel = FindObjectOfType<InfoPanel> ();


		if (flintStrike == 0 && oilPickedUp == 0) 
		{
			flintStrike = Random.Range (1, 4);  // (1, 5) // choose a random number between 1-4 - 5 is excluded from the range// choose a random number between 1-4 - 5 is excluded from the range
			oilPickedUp = Random.Range (2f, 10f);  //(10.0f, 45.0f)
		}
	}



	// ____COLLECTION____
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player)
		{

			//  ____OIL COLLECTION____
			// Pick up oil(oilPickedUp) if oil(oilAmount) is not full (oilMaxHold)
			// player can only fill oil to its max.
			// if the pickup exceeds the max, the remaining oil is left behind (to be collected), otherwise the pickup is depleted and then destroyed.
			if (gameObject.CompareTag ("Oil")) 
			{
				if (lanternController.lantern == null)
				{
					if (lanternController.oilMaxHold > lanternController.oilAmount) 
					{
						if (lanternController.oilAmount + oilPickedUp > lanternController.oilMaxHold) 
						{
							oilPickedUp = lanternController.oilMaxHold - lanternController.oilAmount;
							lanternController.oilAmount = lanternController.oilMaxHold;	
							playerSounds.oilPickUpSound (1);
						} 
						else 
						{

							enterMazeDetector.oilResources.Remove (gameObject);
							lanternController.oilAmount += oilPickedUp;
							if ((gameObject.transform.parent != null) && (gameObject.transform.parent.CompareTag("ResourceSpawn")))
								resourceCheck.isFilled = false;
							playerSounds.oilPickUpSound (0);
							Destroy (gameObject);
							infoPanel.panelIn ();
							//if (enterMazeDetector.oilResources.Count == enterMazeDetector.oilTotal - 1) 
							//{
								//Debug.Log ("I PICKED UP MY FIRST OIL");
							//}		
						}
					}
				}
				else
					dialogueDisplay.ShowBox("I need a lantern to put to oil in");
			}
			if (gameObject.CompareTag ("Flint")) 
			{
				//  ____FLINT COLLECTION____
				// Flint amount is the amount of flint strikes avaliable?
				//if (lanternController.flintMaxHold > lanternController.flintAmount) 
				if (lanternController.flintAmount != lanternController.flintMaxHold) 
				{
					lanternController.flintAmount++;
					if (lanternController.flintStrikeAmount [i] == 0) 
					{
						lanternController.flintStrikeAmount [i] = flintStrike;
						playerSounds.flintPickUpSound ();
						lanternController.flintRepresentation [i].transform.localScale = new Vector3 ((lanternController.flintStrikeAmount [i] / 3f), (lanternController.flintStrikeAmount [i] / 3f), 1f);
						//lanternController.flintRepresentation [i].transform.localScale = new Vector3 ((lanternController.flintStrikeAmount [i] / 2f), (lanternController.flintStrikeAmount [i] / 2f), 1f);
					}
					if ((gameObject.transform.parent != null) && (gameObject.transform.parent.CompareTag("ResourceSpawn")))
						resourceCheck.isFilled = false;
					enterMazeDetector.flintResources.Remove(gameObject);
					Destroy (gameObject);
					infoPanel.panelIn ();
				} 
				else
					dialogueDisplay.ShowBox("I can't hold anymore");
			}
		}
	}

	void FixedUpdate ()
	{
		if (i < lanternController.flintStrikeAmount.Length) 
		{	
			if (lanternController.flintStrikeAmount [i] != 0)
				i++;
		} 
		else
			i = 0;
	}
}
