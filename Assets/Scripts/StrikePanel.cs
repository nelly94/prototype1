﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StrikePanel : MonoBehaviour 
{

	private Animator animator;
	public bool strikePanelIn;


	void Start ()
	{

		animator = this.gameObject.GetComponent<Animator>();
		animator.SetBool("StrikePanelIn", true);
		animator.enabled = true;
		strikePanelIn = true;
	}

	public void Update () 
	{

		if (Input.GetKeyDown (KeyCode.H) && !strikePanelIn)
			StrikePanelIn ();
		else if (Input.GetKeyDown (KeyCode.H) && strikePanelIn) 
			StrikePanelOut ();
	}

	public void StrikePanelIn()
	{
		//animator.enabled = true;	
		animator.SetBool("StrikePanelIn", true);
		animator.Play ("StrikePanelIn");

		strikePanelIn = true;
	}

	public void StrikePanelOut()
	{
		animator.SetBool("StrikePanelIn", false);
		animator.Play ("StrikePanelOut");

		strikePanelIn = false;
	}
}
