// ____ REFERENCES ____
// adapted from Sebastian Lague's "Field of view visualisation" tutorials https://www.youtube.com/playlist?list=PLFt_AvWsXl0dohbtVgHDNmgZV_UY7xZv7 

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
	public float speed;

	// UI
	private StrikePanel strikePanel;
	public GameObject[] lifeRepresentation;
	public int life;
	Image LifeDisplay;

	private Rigidbody rigidbody;
	private Camera viewCamera;
	public Vector3 velocity;

	//private float angle;
	private GameManager gameManager;
	private FadeScreen fadeScreen;
	private string sceneName;

	private TutorialPanel tutorialPanel;
	private LoosePages loosePage;


	void Awake ()
	{
		//rigidbody = gameObject.AddComponent<Rigidbody>();
		rigidbody = GetComponent<Rigidbody> ();
		viewCamera = Camera.main;

		strikePanel = FindObjectOfType<StrikePanel> ();
		life = lifeRepresentation.Length; // Currently have three lives) 
		//LifeDisplay = GetComponent<Image>();

		//playerAnimator = GameObject.FindGameObjectWithTag ("PlayerIcon").GetComponent<Animator> ();

		//gameManager = FindObjectOfType<GameManager> ();
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

		fadeScreen = GameObject.FindWithTag ("FadeTo").GetComponent<FadeScreen> ();

		tutorialPanel = GameObject.FindWithTag ("Tutorial").GetComponent<TutorialPanel> ();
		loosePage = tutorialPanel.gameObject.GetComponentInChildren<LoosePages> ();
	}
		

	void Update ()
	{
		if (!GameManager.Instance.Paused) 
		{
			
			/*
			//Rotating the light according to mouse location 
			Vector3 mousePosition = viewCamera.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));
			transform.LookAt (mousePosition + Vector3.up * transform.position.y);
			*/
						 
			velocity = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized * speed;
			transform.LookAt(transform.position + velocity);

		}
	}

	void FixedUpdate() 
	{
		if (!GameManager.Instance.Paused) 
		{
			//transform.eulerAngles = new Vector3 (transform.eulerAngles.x, angle, transform.eulerAngles.z);
			rigidbody.MovePosition (rigidbody.position + velocity * Time.fixedDeltaTime);
		}

	}

	public void AdjustLife()
	{
		// hardcoding for 3 lives for time sake
		if (life == 3) 
		{ 
			for (int index = 0; index < lifeRepresentation.Length; index++) 
			{
				lifeRepresentation [index].GetComponent<Image> ().enabled = true;
			}
		} 
		else if (life == 2) 
		{
			lifeRepresentation [0].GetComponent<Image> ().enabled = false;
		} 
		else if (life == 1) 
		{
			lifeRepresentation [1].GetComponent<Image> ().enabled = false;
		}
		else 
		{
			lifeRepresentation [2].GetComponent<Image> ().enabled = false;
			gameManager.PauseGame ();
			sceneName = "GameOver";
			StartCoroutine (ChangeScenes());
			//Debug.Log ("GAME OVER");
		}
	}

	private IEnumerator ChangeScenes()
	{
		fadeScreen.Fade (true, 3f);
		while (fadeScreen.fadeImage.color.a < 1) 
		{
			yield return null;
		}
		SceneManager.LoadScene (sceneName);
	}

	/*
	void OnTriggerEnter(Collider other)
	{
		// detect the trigger of the lantern - if collided with collect the lantern and make a light component within the player.
		if (other.gameObject.tag == "Door(Exit)") 
		{
			sceneName = "Victory";
			StartCoroutine (ChangeScenes());
		}
		if (other.gameObject.tag == "Detector2") 
		{
			if (!loosePage.page6Seen) 
			{
				if (!loosePage.gameObject.transform.GetChild (0).gameObject.activeSelf) 
				{
					tutorialPanel.panelIn ();
					loosePage.LoosePageSix ();
					loosePage.page6Seen = true;
					//Destroy (other.gameObject);
				}
			} 
			else 
			{
				if (!loosePage.loosePages [5].gameObject.activeSelf)
					Destroy (other.gameObject);
			}
		}
	}
		*/


}