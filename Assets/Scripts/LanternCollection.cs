﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternCollection : MonoBehaviour 
{
	private GameObject player;
	private LanternController lanternController;
	private PlayerSounds playerSounds;

	void Awake()
	{
		player = GameObject.FindWithTag("Player");
		lanternController = player.GetComponent<LanternController>();
		playerSounds = player.GetComponent<PlayerSounds> ();
	}

	// ____COLLECTION____
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject == player) 
		{
			playerSounds.lanternPickUpSound ();
			lanternController.lightSource.enabled = true;

			lanternController.lightSource.intensity = 8.0f;
			lanternController.lightSource.enabled = false;
			Destroy(gameObject);

		}
	}
}
