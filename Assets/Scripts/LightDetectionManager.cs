﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class LightDetectionManager : MonoBehaviour 
{
	public bool isInReach;
	public MeshRenderer rendererMesh;
	//public Light light;
	public GameObject light;

	private GameObject player;
	private LanternController lanternController;
	private PlayerSounds playerSounds;

	private SpriteRenderer spriteRenderer;
	private Color spriteColor;

	public AudioSource audioSource;
	//public AudioClip[] fireSounds;
	public AudioClip fireSound;

	void Awake()
	{
		rendererMesh = GetComponent<MeshRenderer> ();
		//light = GetComponentInChildren<Light> ();
		light = this.transform.FindChild ("Light").gameObject;

		player = GameObject.FindGameObjectWithTag ("Player");
		lanternController = player.GetComponent<LanternController> ();
		playerSounds = player.GetComponent<PlayerSounds> ();

		spriteRenderer = this.GetComponentInChildren<SpriteRenderer> ();
		//spriteColor = this.spriteRenderer.color;

		audioSource = this.gameObject.GetComponent<AudioSource> ();

		if ((rendererMesh.enabled) && (light.active)) 
		{
			PlayFireSound ();
		}			

		if ((!rendererMesh.enabled) && (!light.active)) 
			this.spriteRenderer.color = new Color (1f, 1f, 1f, 0.65f);
	}

	void OnTriggerStay (Collider other)
	{
		if (other.gameObject == player) 
		{
			isInReach = true;		
			//if (!light.activeSelf) 
				//this.spriteRenderer.color = new Color (1f, 1f, 1f, 0.8f);
		}			
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject == player) 
		{
			isInReach = false;

			if (!light.activeSelf)
				inactiveFade ();
			//	this.spriteRenderer.color = new Color (1f, 1f, 1f, 0.65f);
				//this.spriteRenderer.color = 0.65f;
		}
	}

	public void TurnLightOn()
	{
		if ((!rendererMesh.enabled) && (!light.active)) 
		{
			if (lanternController.flintAmount > 0) 
			{
				lanternController.StrikeFlint (); // this includes the sound
				//Debug.Log ("turn the light on");
				rendererMesh.enabled = true;
				this.spriteRenderer.color = new Color (1f, 1f, 1f, 1f);
				//light.enabled = true;
				light.SetActive (true);

				audioSource.enabled = true;
				PlayFireSound ();
			}
		} 
	}

	public void TurnLightOff()
	{
		if ((rendererMesh.enabled) && (light.active)) 
		{	
			playerSounds.PlayBlowOutSound ();

			inactiveFade ();
			audioSource.enabled = false;
			rendererMesh.enabled = false;
			light.SetActive (false);
			//light.enabled = false;
		}
	}
		
	public void activateFade()
	{
		this.spriteRenderer.color = new Color (1f, 1f, 1f, 0.8f);
	}
		
	public void inactiveFade()
	{
		this.spriteRenderer.color = new Color (1f, 1f, 1f, 0.65f);
	}

	public void PlayFireSound()
	{
		float volume = Random.Range(0.75f, 0.98f);
		//audioSource.clip = this.fireSounds[Random.Range(0,this.fireSounds.Length)];
		audioSource.clip = fireSound;
		//audioSource.time = (Random.Range(0, audioSource.clip.length));
		audioSource.volume = volume;
		audioSource.Play ();
		audioSource.time = (Random.Range(0, audioSource.clip.length));
		//audioSource.PlayOneShot (audioSource.clip, volume);
	}
}

/*
 
void OnMouseOver()
{
	if (!EventSystem.current.IsPointerOverGameObject ()) 
	{
		Debug.Log ("I'm over an object"); 
		if (lightDetection.isInReach) 
		{
			Cursor.SetCursor (cursor, Vector2.zero, CursorMode.Auto);
			if ((!lightDetection.rendererMesh.enabled) && (!lightDetection.light.enabled)) 
			{
				if (Input.GetKeyDown (KeyCode.E) && (lanternController.flintAmount > 0)) 
				{
					lanternController.StrikeFlint ();
					Debug.Log ("turn the light on");
					lightDetection.rendererMesh.enabled = true;
					lightDetection.light.enabled = true;
				}
			} 
			else {
				if (Input.GetKeyDown (KeyCode.E)) 
				{
					Debug.Log ("turn the light off");
					lightDetection.rendererMesh.enabled = false;
					lightDetection.light.enabled = false;
				}				
			}
		} 
		else
			Cursor.SetCursor (null, Vector2.zero, CursorMode.Auto);
	}
}
*/
