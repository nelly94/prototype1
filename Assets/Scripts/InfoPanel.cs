﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InfoPanel : MonoBehaviour 
{
	
	private Animator animator;
	public bool PanelIn;
	public bool hasBeenSeen;

	public GameObject flintGlow;
	public GameObject oilGlow;

	void Start ()
	{

		animator = this.gameObject.GetComponent<Animator>();
		animator.enabled = false;
		hasBeenSeen = false;
	}

	public void Update () 
	{
		
		if (Input.GetKeyDown (KeyCode.I) && !PanelIn) 
			panelIn ();
		else if (Input.GetKeyDown (KeyCode.I) && PanelIn) 
			panelOut ();
	}

	public void panelIn()
	{
		animator.enabled = true;	
		animator.SetBool("PanelIn", true);
		animator.Play ("PanelIn");

		PanelIn = true;
	}

	public void panelOut()
	{
		animator.SetBool("PanelIn", false);
		animator.Play ("PanelOut");

		PanelIn = false;
	}
}
