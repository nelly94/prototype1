﻿// ____ REFERENCES ____
// adapted from N3K EN's "Fade in, fade out - Unity 3D[Tutorial][C#]" tutorial https://www.youtube.com/watch?v=nR5DaSS_orM

using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FadeScreen : MonoBehaviour 
{
	public static FadeScreen Instance{ set; get; }

	public Image fadeImage;
	public bool isTransitioning;
	public float transition;
	private bool isShowing;
	private float duration;

	//void Awake()
	//{
		//Instance = this;
	//}

	public void Fade(bool showing, float duration)
	{
		isShowing = showing;
		isTransitioning = true;
		this.duration = duration;
		transition = (isShowing) ? 0 : 1;
	}

	void Update()
	{
		/*
		if (Input.GetKeyDown (KeyCode.F))
			Fade (true, 3.0f);
		if (Input.GetKeyDown (KeyCode.G))
			Fade (false, 3.0f);
			*/
		
		transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
		fadeImage.color = Color.Lerp (new Color (0,0,0,0), Color.black , transition);

		if (transition > 1 || transition < 0)
			isTransitioning = false;

		if (fadeImage.color.a == 0f)
			fadeImage.gameObject.SetActive (false);
		else
			fadeImage.gameObject.SetActive(true);
	}
}

/*
public static FadeScreen Instance{ set; get; }

	public Image fadeImage;
	public bool isTransitioning;
	private float transition;
	private bool isShowing;
	private float duration;

	//Colour
	private float red;
	private float green;
	private float blue;

	void Awake()
	{
		Instance = this;
	}

	public void Fade(bool showing, float duration, float r, float g, float b)
	{
		isShowing = showing;
		isTransitioning = true;
		this.duration = duration;
		transition = (isShowing) ? 0 : 1;
		red = r;
		green = g;
		blue = b;


	}

	void Update()
	{

		transition += (isShowing) ? Time.deltaTime * (1 / duration) : -Time.deltaTime * (1 / duration);
		fadeImage.color = Color.Lerp (new Color (red, blue, green, 0), new Color(red,blue,green,1) , transition);

		if (transition > 1 || transition < 0)
			isTransitioning = false;
	}
*/
