﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour 
{
	public float minDistance;
	public float attackTime;
	public float attackTimer;

	private FieldOfView fieldOfView;
	private EnemySight enemySight;

	private GameObject player;
	private PlayerController playerController;
	private GameObject playerLight;

	private StrikePanel strikePanel;

	private CameraShake cameraShake;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("PlayerController");
		playerController = player.GetComponent<PlayerController> ();
		playerLight = GameObject.FindWithTag("PlayerLight");
		//visibility = GameObject;
		//enemySight = GetComponentInParent<EnemySight> ();
		enemySight = this.transform.parent.parent.gameObject.GetComponentInParent<EnemySight> ();
		fieldOfView = FindObjectOfType<FieldOfView> ();

		strikePanel = FindObjectOfType<StrikePanel> ();
		cameraShake = FindObjectOfType<CameraShake> ();

		attackTime = 3f;
		attackTimer = 0f;
	}

	void OnTriggerStay (Collider other)
	{ 
		if (!GameManager.Instance.Paused) 
		{
			if (other.gameObject == player || other.gameObject == playerLight) 
			{
				//Debug.Log ("The player is here");
				Vector3 directionToTarget = other.transform.position - transform.position;
				float distanceToTarget = Vector3.Distance (transform.position, other.transform.position);

				//RaycastHit hit;

				// raycast cast towards the player hits something	(direction of a raycast is always normalised to always have a magnitude of one)
				//if (Physics.Raycast (transform.position, directionToTarget.normalized, out hit, 3.25f))
				if (!Physics.Raycast (transform.position, directionToTarget.normalized, distanceToTarget, fieldOfView.obstacleMask)) 
				{
					//if(hit.collider.gameObject == player) 
					//if (hit.collider.gameObject == player || hit.collider.gameObject == playerLight) 
					float distanceBetween = Vector3.Distance (enemySight.transform.position, other.transform.position);
					
					//Debug.Log ("distance" + distanceBetween);
					//Debug.Log ("collision");
					enemySight.position = player.transform.position;
					//transform.LookAt (player.transform); // 
					enemySight.transform.LookAt (player.transform);



					//if (distanceBetween <= minDistance) 
					//{
						attackTimer += Time.deltaTime;

						if (attackTimer >= attackTime) 
						{
							cameraShake.shakecamera ();
							playerController.life--;
							playerController.AdjustLife ();
							attackTimer = 0f;
							if (!strikePanel.strikePanelIn)
								strikePanel.StrikePanelIn ();		
						}
					//}	
				}
			}
		}
	}
	void OnTriggerExit ()
	{
		attackTimer = 0f;
	}
}

