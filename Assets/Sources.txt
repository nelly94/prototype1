Sound

"Sneaky Adventure" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Sneaky Snitch" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Night Time Sound Effect" DigitalDials
Creative Commons Attribution license (reuse allowed)
https://www.youtube.com/watch?v=vDahBTdgaMk


Font

"Haru Biru 2" Haslinda Adnan.
Free to personal use.
http://www.1001fonts.com/haru-biru-2-font.html

"Daniel Font" Daniel Midgley
Free for personal and commercial use.
http://www.1001fonts.com/daniel-font.html

(Glowing Font)
"GLOW STYLES V.2" Idered
Creative Commons: NoDerivs
http://psstyles101.com/view/227/Text/Glow_Styles_v2

