﻿// ____ REFERENCES ____
// adapted from Sebastian Lague's "Field of view visualisation" tutorials https://www.youtube.com/playlist?list=PLFt_AvWsXl0dohbtVgHDNmgZV_UY7xZv7

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (FieldOfView))]
public class FieldOfViewEditor : Editor 
{

	void OnSceneGUI() 
	{
		FieldOfView fieldofview = (FieldOfView)target;
		Handles.color = Color.white;
		Handles.DrawWireArc (fieldofview.transform.position, Vector3.up, Vector3.forward, 360, fieldofview.viewRadius);

		Vector3 viewAngleA = fieldofview.DirFromAngle (-fieldofview.viewAngle / 2, false);
		Vector3 viewAngleB = fieldofview.DirFromAngle (fieldofview.viewAngle / 2, false);
		Handles.DrawLine (fieldofview.transform.position, fieldofview.transform.position + viewAngleA * fieldofview.viewRadius);
		Handles.DrawLine (fieldofview.transform.position, fieldofview.transform.position + viewAngleB * fieldofview.viewRadius);



		//foreach (Transform visibleTarget in fieldofview.visibleTargets)
		//{
			//Handles.color = Color.red;
			//Handles.DrawLine (fieldofview.transform.position, visibleTarget.position);
		//}

	}
}
